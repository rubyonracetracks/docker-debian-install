#!/bin/bash

BRANCH=''
OS_RELEASE=`cat /etc/os-release`

# Fill in DOCKER_IMAGE and CONTAINER parameters
grep_release () {
  STR=$1
  BR=$2
  IS_RELEASE=`cat /etc/os-release | grep "$STR"`
  if [[ "$IS_RELEASE" != '' ]]; then
    BRANCH="$BR"
  fi
}

# Automatically determine which branch of Debian is installed

# For SparkyLinux 4
grep_release 'tyche' 'stretch'
grep_release 'Tyche' 'stretch'

# For SparkyLinux 5
grep_release 'nibiru' 'buster'
grep_release 'Nibiru' 'buster'

# For Debian and MX Linux
grep_release 'stretch' 'stretch'
grep_release 'Stretch' 'stretch'
grep_release 'buster' 'buster'
grep_release 'Buster' 'buster'

# If necessary, ask the user which branch of Debian is installed
while [[ "$BRANCH" != 'stretch' && "$BRANCH" != 'buster' ]]; do
  if [[ "$BRANCH" != 'stretch' && "$BRANCH" != 'buster' ]]; then
    echo 'Which branch of Debian is installed on your system?'
    echo "Enter 'stretch' to select Debian Stretch."
    echo "Enter 'buster' to select Debian Buster."
    read BRANCH
  fi
done

echo '***********************'
echo 'BEGIN installing Docker'
echo '***********************'

echo '------------------------------------'
echo 'sudo apt-get install -y libapparmor1'
sudo apt-get install -y libapparmor1

echo '--------------------------------------'
echo 'sudo apt-get install -y cgroupfs-mount'
sudo apt-get install -y cgroupfs-mount

echo '-------------------------------------------------------------------------------------------'
echo 'sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common'
sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common

echo '----------------------------------------------------------------------------'
echo 'curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -'
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

echo '------------------------------------------'
echo 'Adding /etc/apt/sources.list.d/docker.list'

SOURCE_DOCKER="deb https://download.docker.com/linux/debian $BRANCH stable"
echo $SOURCE_DOCKER | sudo tee /etc/apt/sources.list.d/docker.list
sudo apt-get update

# Install Docker
echo '--------------------'
echo 'Installing docker-ce'
sudo apt-get -y install docker-ce

# Provide non-root access to Docker
echo '---------------------------------'
echo 'Provide non-root access to Docker'
echo
echo 'sudo groupadd docker'
sudo groupadd docker

echo "sudo gpasswd -a ${LOGNAME} docker"
sudo gpasswd -a ${LOGNAME} docker

echo '---------------------------'
echo 'sudo service docker restart'
sudo service docker restart

wait

# Install Docker Compose
bash install-docker-compose.sh

wait

echo '***********************************'
echo 'FINISHED installing Docker (mostly)'
echo '***********************************'

bash docker-version.sh
