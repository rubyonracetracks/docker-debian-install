#!/bin/bash

DIR_PWD=$PWD

cd $DIR_PWD && bash install-docker-only.sh

echo '----------------------------------------------------------------'
echo 'git clone https://gitlab.com/rubyonracetracks/install-host-tools'
git clone https://gitlab.com/rubyonracetracks/install-host-tools
cd install-host-tools && bash main.sh

cd $DIR_PWD && bash docker-version.sh
