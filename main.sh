#!/bin/bash

DATE=`date +%Y%m%d-%H%M%S`

mkdir -p log
bash exec-main.sh 2>&1 | tee log/log-$DATE.txt
