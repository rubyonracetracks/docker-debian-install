#!/bin/bash

echo '--------------'
echo 'docker version'
docker version

echo '-----'
echo 'NOTE:'
echo "If Docker gives an error message, don't worry about it."
echo 'This is normal at this point, because you are not quite finished.'
echo ''
echo '--------------'
echo 'Please reboot.'
echo 'This the last step of installing Docker.'
